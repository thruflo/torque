# DOCKER-VERSION 0.6.3
FROM    ubuntu:precise
RUN     echo "deb http://archive.ubuntu.com/ubuntu precise main universe" \
        > /etc/apt/sources.list
RUN     apt-get update

# Install dependencies.
RUN     apt-get install build-essentials
RUN     apt-get install -y nginx
RUN     apt-get install -y openssh-server
RUN     apt-get install -y libevent
RUN     apt-get install -y python3.2
RUN     apt-get install -y python-dev
RUN     apt-get install -y python-pip

# Bundle app.
ADD     ./app /src

# Install app dependencies.
RUN     pip install setuptools-git
RUN     cd /app; pip install -r requirements.txt

# Run and expose the app.
EXPOSE  8080
CMD     ["gunicorn", "-c", "/src/etc/gunicorn.py", "torque.main:app"]
