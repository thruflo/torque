import logging
import os
import signal
import sys

from pyramid.settings import asbool

def _on_starting(server):
    import gevent.monkey
    gevent.monkey.patch_socket()

def _when_ready(server):
    def monitor():
        modify_times = {}
        while True:
            for module in sys.modules.values():
                path = getattr(module, "__file__", None)
                if not path: continue
                if path.endswith(".pyc") or path.endswith(".pyo"):
                    path = path[:-1]
                try:
                    modified = os.stat(path).st_mtime
                except:
                    continue
                if path not in modify_times:
                    modify_times[path] = modified
                    continue
                if modify_times[path] != modified:
                    logging.info("%s modified; restarting server", path)
                    os.kill(os.getpid(), signal.SIGHUP)
                    modify_times = {}
                    break
            gevent.sleep(0.67)
    
    import gevent
    gevent.spawn(monitor)


backlog = int(os.environ.get('GUNICORN_BACKLOG', 64))
bind = '0.0.0.0:{0}'.format(os.environ.get('PORT', 5100))
daemon = asbool(os.environ.get('GUNICORN_DAEMON', False))
max_requests = int(os.environ.get('GUNICORN_MAX_REQUESTS', 24000))
mode = os.environ.get('INI_mode', 'development')
preload_app = asbool(os.environ.get('GUNICORN_PRELOAD_APP', False))
proc_name = os.environ.get('GUNICORN_PROC_NAME', 'pyramid')
timeout = int(os.environ.get('GUNICORN_TIMEOUT', 10))
workers = int(os.environ.get('GUNICORN_WORKERS', 3))
worker_class = os.environ.get('GUNICORN_WORKER_CLASS', 'gevent')

if 'gevent' in worker_class.lower():
    if mode == 'development':
        on_starting = _on_starting
        when_ready = _when_ready

