from setuptools import setup, find_packages

setup(
    name='torque',
    version='2.0',
    description = 'Web hook task queue service.',
    classifiers=[
        'Development Status :: 3 - Alpha',
        'Environment :: Web Environment',
        'Framework :: Pylons',
        'Natural Language :: English',
        'Operating System :: OS Independent',
        'Programming Language :: Python :: 3.2',
        'Topic :: Internet :: WWW/HTTP :: WSGI :: Application'
    ],
    author='James Arthur',
    author_email='username: thruflo, domain: gmail.com',
    url = 'http://github.com/thruflo/torque2',
    packages = find_packages('src'),
    package_dir = {'': 'src'},
    include_package_data=True,
    entry_points = """\
        [setuptools.file_finders]
        ls = setuptools_git:gitlsfiles
    """
)
